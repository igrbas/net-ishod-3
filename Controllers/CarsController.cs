using Microsoft.AspNetCore.Mvc;
using CarDealership.Models;

namespace CarDealership.Controllers {

    [ApiController]
    [Route("api/[controller]")]
    public class CarsController : ControllerBase {

        public CarsController(ICarService carService) {
            this.carService = carService;
        }

        [HttpGet]
        public async Task<IActionResult> GetCars() {
            try {
                return Ok(await carService.All());
            } catch (Exception) {
                return StatusCode(500);
            }
        }

        [HttpGet("make/{make}")]
        public async Task<IActionResult> GetCarsByMark(string make) {
            try{
                return Ok(await carService.FindByMake(make));
            } catch (Exception) {
                return StatusCode(500);
            }
        }

        [HttpGet("{id}")]
        public async Task<IActionResult> GetCar(int id) {
            try {
                var car = await carService.Get(id);
                if(car == null) return NotFound();
                return Ok(car);
            } catch (Exception) {
                return StatusCode(500);
            }
        }

        [HttpPost]
        public async Task<IActionResult> CreateCar(Car car) {
            try {
                int result = await carService.Create(car);
                if(result == -1) return BadRequest();
                return Created($"/{car.Id}", car);
            } catch (Exception) {
                return StatusCode(500);
            }
        }

        [HttpPut("{id}")]
        public async Task<IActionResult> UpdateCar(int id, Car car) {
            if(car.Id != id) return BadRequest();
            try {
                bool result = await carService.Update(car);
                if(!result) return NotFound();
                return Ok();
            } catch (Exception) {
                return StatusCode(500);
            }
        }

        [HttpDelete("{id}")]
        public async Task<IActionResult> DeleteCar(int id) {
            try {
                int result = await carService.Delete(id);
                if(result < 1) return NotFound();
                return Ok();
            } catch (Exception) {
                return StatusCode(500);
            }
        }

        private readonly ICarService carService;
    }
}