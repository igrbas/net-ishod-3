using Microsoft.AspNetCore.Mvc;
using CarDealership.Models;

namespace CarDealership.Controllers {

    [ApiController]
    [Route("api/[controller]")]
    public class StoresController : ControllerBase {

        public StoresController(IStoreService ss, ICarService cs) {
            storeService = ss;
            carService = cs;
        }

        [HttpGet]
        public async Task<IActionResult> GetStores() {
            try {
                return Ok(await storeService.All());
            } catch (Exception) {
                return StatusCode(500);
            }
        }

        [HttpGet("{id}")]
        public async Task<IActionResult> GetStore(int id) {
            try {
                var store = await storeService.Get(id);
                if(store == null) return NotFound();
                return Ok(store);
            } catch (Exception) {
                return StatusCode(500);
            }
        }

        [HttpGet("{id}/cars")]
        public async Task<IActionResult> GetStoreCars(int id) {
            try {
                return Ok(await carService.FindByStore(id));
            } catch (Exception) {
                return StatusCode(500);
            }
        }

        [HttpGet("{storeId}/cars/{carId}")]
        public async Task<IActionResult> GetStoreCar(int storeId, int carId) {
            try {
                var store = await storeService.Get(storeId);
                if(store == null) return NotFound();
                var car = await carService.GetStoreCar(storeId, carId);
                if(car == null) return NotFound();
                return Ok(car);
            } catch (Exception) {
                return StatusCode(500);
            }
        }

        [HttpPost]
        public async Task<IActionResult> CreateStore(Store store) {
            try {
                int result = await storeService.Create(store);
                if(result == -1) return BadRequest();
                return Created($"/{store.Id}", store);
            } catch (Exception) {
                return StatusCode(500);
            }
        }

        [HttpPost("{id}/cars")]
        public async Task<IActionResult> CreateStoreCar(int id, Car car) {
            if(id != car.StoreId) return BadRequest();
            
            try {
                var store = await storeService.Get(id);
                if(store == null) return NotFound();
                int result = await carService.Create(car);
                if(result == -1) return BadRequest();
                return Created($"{id}/cars/{car.Id}", car);
            } catch (Exception) {
                return StatusCode(500);
            }
        }

        [HttpPut("{id}")]
        public async Task<IActionResult> UpdateStore(int id, Store store) {
            if(store.Id != id) return BadRequest();
            try {
                bool result = await storeService.Update(store);
                if(!result) return NotFound();
                return Ok();
            } catch (Exception) {
                return StatusCode(500);
            }
        }

        [HttpPut("{storeId}/cars/{carId}")]
        public async Task<IActionResult> UpdateStoreCar(int storeId, int carId, Car car) {
            if(car.Id != carId || car.StoreId != storeId) return BadRequest();

            try {
                var store = await storeService.Get(storeId);
                if(store == null) return NotFound();
                bool result = await carService.Update(car);
                if(!result) return NotFound();
                return Ok();
            } catch (Exception) {
                return StatusCode(500);
            }
        }

        [HttpDelete("{id}")]
        public async Task<IActionResult> DeleteStore(int id) {
            try {
                int result = await storeService.Delete(id);
                if(result < 1) return NotFound();
                return Ok();
            } catch (Exception) {
                return StatusCode(500);
            }
        }

        [HttpDelete("{storeId}/cars/{carId}")]
        public async Task<IActionResult> DeleteStoreCar(int storeId, int carId) {
            try {
                var store = await storeService.Get(storeId);
                if(store == null) return NotFound();
                int result = await carService.DeleteStoreCar(storeId, carId);
                if(result < 1) return NotFound();
                return Ok();
            } catch (Exception) {
                return StatusCode(500);
            }
        }

        private readonly IStoreService storeService;
        private readonly ICarService carService;
    }
}