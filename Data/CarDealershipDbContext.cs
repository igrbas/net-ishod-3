using Microsoft.EntityFrameworkCore;
using CarDealership.Models;

#nullable disable

public class CarDealershipDbContext : DbContext {

    public CarDealershipDbContext(DbContextOptions<CarDealershipDbContext> options) : base(options) {}

    public DbSet<Car> Cars { get; set; }

    public DbSet<Store> Stores { get; set; }
}