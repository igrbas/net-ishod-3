using System.ComponentModel.DataAnnotations;

namespace CarDealership.Models {

    public class Car {

        public int Id { get; set; }

        [Required]
        [StringLength(100)]
        public string? Make { get; set; }

        [Required]
        [StringLength(100)]
        public string? Model { get; set; }

        [Required]
        [Range(1930,2022)]
        public int Year { get; set; }

        [Required]
        [Range(0,20000000)]
        public double Price { get; set; }

        [Required]
        public bool Drivable { get; set; }

        public DateTime? RegistrationExpiry { get; set; }

        [Required]
        [Range(0,500000)]
        public int Mileage { get; set; }

        [Required]
        public int StoreId { get; set; }
    }
}