using Microsoft.EntityFrameworkCore;

namespace CarDealership.Models {

    public class CarService : ICarService {

        public CarService(CarDealershipDbContext context) {
            Db = context;
        }

        public async Task<IEnumerable<Car>> All() {
            return await Db.Cars.ToListAsync();
        }

        public async Task<IEnumerable<Car>> FindByStore(int storeId) {
            return await Db.Cars.Where(car => car.StoreId == storeId).ToListAsync();
        }

        public async Task<IEnumerable<Car>> FindByMake(string make) {
            return await Db.Cars.Where(car => car.Make == make).ToListAsync();
        }

        public async Task<Car?> Get(int id) {
            return await Db.Cars.FirstOrDefaultAsync(car => car.Id == id);
        }

        public async Task<Car?> GetStoreCar(int storeId, int carId) {
            return await Db.Cars.FirstOrDefaultAsync(car => car.Id == carId && car.StoreId == storeId);
        }

        public async Task<int> Create(Car? car) {
            if(car == null) return -1;
            Db.Add(car);
            return await Db.SaveChangesAsync();
        }

        public async Task<bool> Update(Car? car) {
            if(car == null) return false;

            try {
                Db.Update(car);
                await Db.SaveChangesAsync();
                return true;
            } catch (DbUpdateConcurrencyException) {
                return false;
            }
        }

        public async Task<int> Delete(int id) {
            var car = await Get(id);
            if(car == null) return -1;

            Db.Cars.Remove(car);
            return await Db.SaveChangesAsync();
        }

        public async Task<int> DeleteStoreCar(int storeId, int carId) {
            var car = await GetStoreCar(storeId, carId);
            if(car == null) return -1;

            Db.Cars.Remove(car);
            return await Db.SaveChangesAsync();
        }

        public CarDealershipDbContext Db { get; set; }
    }
}