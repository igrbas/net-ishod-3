namespace CarDealership.Models {

    public interface ICarService {
        Task<IEnumerable<Car>> All();

        Task<IEnumerable<Car>> FindByStore(int storeId);

        Task<IEnumerable<Car>> FindByMake(string make);

        Task<Car?> Get(int id);

        Task<Car?> GetStoreCar(int storeId, int carId);

        Task<int> Create(Car? car);

        Task<bool> Update(Car? car);

        Task<int> Delete(int id);

        Task<int> DeleteStoreCar(int storeId, int carId);

        CarDealershipDbContext Db { get; set; }
    }
}