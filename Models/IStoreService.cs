namespace CarDealership.Models {
    public interface IStoreService {

        Task<IEnumerable<Store>> All();

        Task<Store?> Get(int id);

        Task<int> Create(Store? store);

        Task<bool> Update(Store? store);

        Task<int> Delete(int id);

        CarDealershipDbContext Db { get; set; }
    }
}