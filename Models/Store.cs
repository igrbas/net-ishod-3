using System.ComponentModel.DataAnnotations;

namespace CarDealership.Models {
 
    public class Store {

        public int Id { get; set; }

        [Required]
        [StringLength(100)]
        public string? Address { get; set; }

        public List<Car>? Cars { get; set; }

    }
}