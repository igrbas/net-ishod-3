using Microsoft.EntityFrameworkCore;

namespace CarDealership.Models {

    public class StoreService : IStoreService {

        public StoreService(CarDealershipDbContext context) {
            Db = context;
        }

        public async Task<IEnumerable<Store>> All() {
            return await Db.Stores.ToListAsync();
        }

        public async Task<Store?> Get(int id) {
            return await Db.Stores.FirstOrDefaultAsync(s => s.Id == id);
        }

        public async Task<int> Create(Store? store) {
            if(store == null) return -1;
            Db.Add(store);
            return await Db.SaveChangesAsync();
        }

        public async Task<bool> Update(Store? store) {
            if(store == null) return false;

            try {
                Db.Update(store);
                await Db.SaveChangesAsync();
                return true;
            } catch (DbUpdateConcurrencyException) {
                return false;
            }
        }

        public async Task<int> Delete(int id) {
            var store = await Get(id);
            if(store == null) return -1;

            Db.Remove(store);
            return await Db.SaveChangesAsync();
        }

        public CarDealershipDbContext Db { get; set; }
    }
}